---
title: "About"
date: 2021-09-18T16:59:34-03:00
draft: false
---

Meu nome é João Gabriel, e eu nasci em Araguari-MG. Com 2 anos de idade, me mudei para Anápolis, cidade no interior de Goiás, onde vivi minha vida inteira. Aos 19, me mudei para São Paulo, a fim de cursar engenharia mecatrônica na Escola Politécnica da USP. Sempre fui muito apaixonado por música, mas hoje encaro apenas como hobby.

