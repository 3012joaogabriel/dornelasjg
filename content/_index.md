 Bem-vindo ao meu mundo de paixão pelo violoncelo!
            <p>Meu nome é João Gabriel, e sou um grande admirador e amante do violoncelo. Este instrumento magnífico tem cativado meu coração e alma com sua beleza e som sublime.</p>
            <p>Explore o meu site para aprender mais sobre o violoncelo, sua história, técnicas de reprodução e descobrir algumas das minhas músicas e interpretações favoritas.</p>
        </div>